import java.util.*;
import java.util.stream.Collectors;

/* CompliantNode refers to a node that follows the rules (not malicious)*/
public class CompliantNode implements Node {
    private final int numRounds;

    private Set<Transaction> proposals;
    private boolean[] followees;

    public CompliantNode(double p_graph, double p_malicious, double p_txDistribution, int numRounds) {
        this.numRounds = numRounds;
    }

    public void setFollowees(boolean[] followees) {
        this.followees = followees;
    }

    public void setPendingTransaction(Set<Transaction> pendingTransactions) {
        this.proposals = pendingTransactions;
    }

    public Set<Transaction> sendToFollowers() {
        return proposals;
    }

    public void receiveFromFollowees(Set<Candidate> candidates) {
        Map<Integer, Set<Transaction>> txsBroadcastedBySenders = groupTransactionsBySenders(candidates);

        blackListSilentSenders(txsBroadcastedBySenders.keySet());

        // drop txs made by silent senders
        txsBroadcastedBySenders = txsBroadcastedBySenders.entrySet().stream()
                .filter(entry -> followees[entry.getKey()])
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        proposals.clear();

        proposals = txsBroadcastedBySenders.values().stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    private void blackListSilentSenders(Set<Integer> senders) {
        for(int i = 0 ; i < followees.length ; i++) {
            if(followees[i]) {
                followees[i] = senders.contains(i);
            }
        }
    }

    private Map<Integer, Set<Transaction>> groupTransactionsBySenders(Set<Candidate> candidates) {
        Map<Integer, Set<Transaction>> txByCandidates = new HashMap<>();
        for(Candidate candidate : candidates){
            txByCandidates.putIfAbsent(candidate.sender, new HashSet<>());
            txByCandidates.get(candidate.sender).add(candidate.tx);
        }

        return txByCandidates;
    }

    private Map<Transaction, Set<Integer>> groupSendersByTransactions(Set<Candidate> candidates) {
        Map<Transaction, Set<Integer>> sndByTx = new HashMap<>();
        for(Candidate c : candidates) {
            sndByTx.putIfAbsent(c.tx, new HashSet<>());
            sndByTx.get(c.tx).add(c.sender);
        }

        return sndByTx;
    }

}
