// Block Chain should maintain only limited block nodes to satisfy the functions
// You should not have all the blocks added to the block chain in memory 
// as it would cause a memory overflow.

import java.util.*;

public class BlockChain {
    public static final int CUT_OFF_AGE = 10;

    private final TransactionPool transactionPool;

    private final TreeMap<Integer, List<ByteArrayWrapper>> headBlocksWithinHeight;

    private final Map<ByteArrayWrapper, UTXOPool> headBlockUTXOPool;

    private final Map<ByteArrayWrapper, Block> blockDictionary;

    // Height of the longest chain
    private int maxHeight;

    /**
     * create an empty block chain with just a genesis block.
     * Assume {@code genesisBlock} is a valid block
     */
    public BlockChain(Block genesisBlock) {
        headBlocksWithinHeight = new TreeMap<>();
        blockDictionary = new HashMap<>();
        headBlockUTXOPool = new HashMap<>();

        transactionPool = new TransactionPool();

        ByteArrayWrapper hash = createByteArray(genesisBlock.getHash());

        // Add block to block dictionary
        blockDictionary.put(hash, genesisBlock);

        // create new UTXO pool to add the coinbase tx outputs
        UTXOPool pool = new UTXOPool();
        headBlockUTXOPool.put(hash, pool);


        // Add genesis block at height 0, TODO: check if it is 0 or 1
        headBlocksWithinHeight.put(1, new ArrayList<>());
        headBlocksWithinHeight.get(1).add(hash);

        // Add coinbase tx outputs to UTXO pool
        pool.addUTXO(new UTXO(genesisBlock.getCoinbase().getHash(), 0),
                genesisBlock.getCoinbase().getOutput(0)
        );
    }

    /**
     * Get the maximum height block
     */
    public Block getMaxHeightBlock() {
        return blockDictionary.get(getHeadBlockHash());
    }

    /**
     * Get the UTXOPool for mining a new block on top of max height block
     */
    public UTXOPool getMaxHeightUTXOPool() {
        // TODO : Implement This
        // TODO: Identify the block with max height and return UTXOPool

        ByteArrayWrapper headBlock = getHeadBlockHash();
        return new UTXOPool(headBlockUTXOPool.get(headBlock));
    }

    private ByteArrayWrapper getHeadBlockHash() {
        return headBlocksWithinHeight.lastEntry().getValue().get(0);
    }


    /**
     * Get the transaction pool to mine a new block
     */
    public TransactionPool getTransactionPool() {
        return transactionPool;
    }

    /**
     * Add {@code block} to the block chain if it is valid. For validity, all transactions should be
     * valid and block should be at {@code height > (maxHeight - CUT_OFF_AGE)}.
     * <p>
     * <p>
     * For example, you can try creating a new block over the genesis block (block height 2) if the
     * block chain height is {@code <= CUT_OFF_AGE + 1}. As soon as {@code height > CUT_OFF_AGE + 1}, you cannot create a new block
     * at height 2.
     *
     * @return true if block is successfully added
     */
    public boolean addBlock(Block block) {
        if (Objects.isNull(block.getPrevBlockHash()))
            return false;

        // Check for cutOff height
        // Only keep blocks with height within cutOff
        if(!blockDictionary.containsKey(createByteArray(block.getPrevBlockHash()))) {
            return false;
        }
        // TODO: Check the validity of the transactions set
        UTXOPool prevUTXOPool = headBlockUTXOPool.get(createByteArray(block.getPrevBlockHash()));

        Objects.requireNonNull(prevUTXOPool, "UTXOPool of previous block couldn't be found even though " +
                "the block is in the dict");

        TxHandler txVerifier = new TxHandler(new UTXOPool(prevUTXOPool));

        Transaction[] validTxs = txVerifier.handleTxs(
                block.getTransactions().toArray(new Transaction[block.getTransactions().size()]));

        if(validTxs.length != block.getTransactions().size())
            return false;

        // TODO: Add block to the blockchain
        int height = getHeight(createByteArray(block.getPrevBlockHash()));

        removeBlocksLessThanCutOffAge(height + 1);

        insertBlockInChain(block, txVerifier.getUTXOPool(), height + 1);

        return true;
    }

    /**
     * Add a transaction to the transaction pool
     */
    public void addTransaction(Transaction tx) {
        this.transactionPool.addTransaction(tx);
    }

    private void removeBlocksLessThanCutOffAge(int maxHeight) {
        int minHeight = headBlocksWithinHeight.firstKey();

        if(minHeight < maxHeight - CUT_OFF_AGE) {
            List<ByteArrayWrapper> deletedHashes = headBlocksWithinHeight.remove(minHeight);
            for (ByteArrayWrapper hash : deletedHashes) {
                blockDictionary.remove(hash);
                headBlockUTXOPool.remove(hash);
            }

        }
    }

    private void insertBlockInChain(Block block, UTXOPool pool, int height) {
        blockDictionary.put(createByteArray(block.getHash()), block);

        headBlockUTXOPool.put(createByteArray(block.getHash()), pool);


        headBlocksWithinHeight.putIfAbsent(height, new ArrayList<>());
        headBlocksWithinHeight.get(height).add(createByteArray(block.getHash()));

        // add coinBase Tx
        Transaction coinBase = block.getCoinbase();
        for (int i = 0 ; i < coinBase.getOutputs().size() ; i++) {
            Transaction.Output output = coinBase.getOutput(i);
            UTXO utxo = new UTXO(coinBase.getHash(), i);
            pool.addUTXO(utxo, output);
        }
    }

    private ByteArrayWrapper createByteArray(byte[] arr) {
        return new ByteArrayWrapper(arr);
    }

    private int getHeight(ByteArrayWrapper hash) {
        for(Map.Entry<Integer, List<ByteArrayWrapper>> entry : headBlocksWithinHeight.entrySet()) {
            int height = entry.getKey();
            for(ByteArrayWrapper currHash : entry.getValue()) {
                if(hash.equals(currHash))
                    return height;
            }
        }
        throw new NoSuchElementException(String.format("this hash is not found in headBlocksWithinHeight" +
                ", Which means that the dictionary is not consistent"));
    }

}